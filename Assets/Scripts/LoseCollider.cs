﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoseCollider : MonoBehaviour
{
    //LevelManager levelManager;

    void Start()
    {
        //levelManager = GameObject.FindObjectOfType<LevelManager>();
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        Destroy(collider.gameObject);
        if (StartManager.playerLives == 0)
        {
            LevelManager.GameOver();
        }
        else
        {
            StartManager.playerLives--;
            LevelManager.LoseLife();
        }
    }
}
