﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour
{

    static MusicPlayer musicPlayer = null;
    // Use this for initialization

    void Awake()
    {
        Debug.Log("Music Player Awake " + GetInstanceID());
        if (musicPlayer != null)
        {
            Destroy(gameObject);
        }
        else
        {
            musicPlayer = this;
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        Debug.Log("Music Player Start " + GetInstanceID());

    }

    // Update is called once per frame
    void Update()
    {

    }
}
