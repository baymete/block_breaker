﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartManager : MonoBehaviour
{
    public static int playerLives = 3;
    void Start()
    {
        LevelStart();
    }


    public void LoadLevel(string name)
    {
        Debug.Log("New Level load: " + name);
        Application.LoadLevel(name);
    }

    public void QuitRequest()
    {
        Debug.Log("Quit requested");
        Application.Quit();
    }

    void LevelStart()
    {
        StartManager.playerLives = 3;
    }

}
