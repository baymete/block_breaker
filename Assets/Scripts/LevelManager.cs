﻿using UnityEngine;
using UnityEngine.UI;   
using System.Collections;

public class LevelManager : MonoBehaviour
{
    static Text playerLivesText;
    static Text middleText;
    static Text gameOverText;
    static Text winText;

    public static bool playerStart;
    static int brickCount;

    public GameObject ball;
    GameObject paddle;

    void Start()
    {
        print("LevelManager start");
        Screen.showCursor = false;
        brickCount = GameObject.FindGameObjectsWithTag("Brick").Length;
        paddle = GameObject.Find("Paddle");
        Text[] texts = GameObject.FindObjectsOfType<Text>();
        foreach (var text in texts) // TODO: Bunun için daha iyi bir şey bulunacak
        {
            if (text.name == "MiddleText")
            {
                middleText = text;
            }
            else if (text.name == "GameOverText")
            {
                gameOverText = text;
            }
            else if (text.name == "PlayerLivesText")
            {
                playerLivesText = text;
            }
            else if (text.name == "WinText")
            {
                winText = text;
            }
        }
        playerStart = false;
        if (gameOverText)
            gameOverText.enabled = false;
        if (middleText)
            middleText.enabled = false;
        if (winText)
            winText.enabled = false;
        if (playerLivesText)
            playerLivesText.text = "Lives: " + StartManager.playerLives;
        playerLivesText.enabled = true;
        var ballInstance = Instantiate(ball, new Vector3(8f, 1.308f, 0f), Quaternion.identity) as GameObject;
    }

    void Update()
    {
        if (brickCount <= 0)
        {
            var ball = GameObject.FindGameObjectWithTag("Ball") as GameObject;
            ball.rigidbody2D.velocity = new Vector2(0f, 0f);
            winText.enabled = true;
            winText.text = "well done";
            playerStart = true;
        }
    }

    public void LoadLevel(string name)
    {
        Debug.Log("New Level load: " + name);
        Application.LoadLevel(name);
    }

    public void NextLevel()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
    }

    public void QuitRequest()
    {
        Debug.Log("Quit requested");
        Application.Quit();
    }

    public static void BrickDestroyed()
    {
        brickCount--;
    }


    public static void LoseLife()
    {
        playerStart = true;
        middleText.enabled = true;
        middleText.text = "You lost a life";
        playerLivesText.text = "Lives: " + StartManager.playerLives;
    }

    public void PlayerStart()
    {
        paddle.transform.position = new Vector3(8f, 1f, 0f);
        var ballInstance = Instantiate(ball, new Vector3(8f, 1.308f, 0f), Quaternion.identity) as GameObject;
        middleText.enabled = false;
        playerStart = false;
    }

    public static void GameOver()
    {
        gameOverText.enabled = true;
        gameOverText.text = "game over";
    }

}
