﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour
{
    Vector3 paddleToBallVector;
    GameObject paddle;
    bool isLaunched;

    void Start()
    {
        paddle = GameObject.Find("Paddle");
        paddleToBallVector = transform.position - paddle.transform.position;
        isLaunched = false;    
    }

    void Update()
    {
        if (!isLaunched)
        {
            transform.position = new Vector3(paddle.transform.position.x, paddle.transform.position.y + paddleToBallVector.y, 0f);

            if (Input.GetMouseButtonDown(0) && !LevelManager.playerStart)
            { 
                rigidbody2D.velocity = new Vector2(2f, 10f);
                isLaunched = true;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
        rigidbody2D.velocity += tweak;
        if (collision.gameObject.tag == "Paddle" && isLaunched)
        {
            audio.Play();
        }
    }
}
