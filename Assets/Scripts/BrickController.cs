﻿using UnityEngine;
using System.Collections;

public class BrickController : MonoBehaviour
{
    public AudioClip crack;
    public int maxHits;
    int timesHit;
    public Sprite[] hitSprites;
    public GameObject smoke;
    // Use this for initialization
    void Start()
    {
        timesHit = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D Collision)
    {
        AudioSource.PlayClipAtPoint(crack, transform.position);
        bool isBreakable = (this.tag == "Brick");
        if (isBreakable)
            HandleHits();
    }

    private void HandleHits()
    {
        timesHit++;
        if (timesHit >= maxHits)
        {
            GameObject smokeInstance = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;
            smokeInstance.particleSystem.startColor = this.GetComponent<SpriteRenderer>().color;
            Destroy(gameObject);
            LevelManager.BrickDestroyed();
        }
        else
        {
            int spriteIndex = timesHit - 1;
            this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
    }
}
