﻿using UnityEngine;
using System.Collections;

public class PaddleController : MonoBehaviour {

	// Use this for initialization
    int screenWidth = Screen.width;
    public bool autoPlay;
    GameObject ball;

	void Start () {
        transform.position = new Vector3(8f, 1f, 0f);
        ball = GameObject.FindGameObjectWithTag("Ball");
	}
	
	// Update is called once per frame
	void Update () {
        if(!autoPlay)
        {
            MoveWithMouse();
        }
        else
        {
            transform.position = new Vector3(ball.transform.position.x, transform.position.y, transform.position.z);
        }
	}

    private void MoveWithMouse()
    {
        float mousePos = Input.mousePosition.x / screenWidth * 16;
        transform.position = new Vector3(Mathf.Clamp(mousePos, 0.5f, 15.5f), 1f, 0f);
    }
}
